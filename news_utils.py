import requests
from bs4 import BeautifulSoup
from airflow.providers.postgres.hooks.postgres import PostgresHook
from psycopg2 import sql
from typing import Union, List


#   Extracts links from a specified website.
#?  @param option: The specific website option for which links are to be extracted.
#$  @return: A list of links extracted from the specified website.

def __get_links__(option):

    url_dict = {
        "pronews": 'https://www.pronews.gr/',
        "bankingnews": 'https://www.bankingnews.gr/',
        "newsit": 'https://www.newsit.gr/',
        "newsbreak": 'https://www.newsbreak.gr/',
        "efsyn": 'https://www.efsyn.gr/',
        "kathimerini": 'https://www.kathimerini.gr/',
        "tovima": 'https://www.tovima.gr/',
        "tanea": 'https://www.tanea.gr/',
        "protothema": 'https://www.protothema.gr/',
        "eleftherostypos": 'https://eleftherostypos.gr/',
        "naftemporiki": 'https://www.naftemporiki.gr/',
        "ethnos": 'https://www.ethnos.gr/',
        "news247": 'https://www.news247.gr/',
        "makeleio": 'https://www.makeleio.gr/',
        "cnn": 'https://www.cnn.gr/',
        "in": 'https://www.in.gr/',
        "newsbomb": 'https://www.newsbomb.gr/',
        "skai": 'https://www.skai.gr/',
        "ieidiseis": 'https://www.ieidiseis.gr/',
        "enikos": 'https://www.enikos.gr/',
        "iefimerida": 'https://www.iefimerida.gr/',
        "alphatv": 'https://www.alphatv.gr/news/',
        "madata": 'https://www.madata.gr/',
        "focusfm": 'https://focusfm.gr/'
    }

    response = requests.get(url_dict[option])

    if response.status_code == 200:
        soup = BeautifulSoup(response.text, 'html.parser')

        links = soup.find_all('a')
        
        for link in links:
            print(link.get('href'))

        return links

    else:
        print(f"Failed to retrieve the page. Status code: {response.status_code}")


#   Extract links for a specified option and save them to a text file.
#?  @param option: The specific option for which links are to be extracted.
#$ @return: links as a list

def __save_links__(option):

    links = __get_links__(option)

    with open(f"{option}.txt", "w") as file:
        for link in links:
            file.write(f"{link}\n")

    return links


#   Insert all links from a file into the 'website_links' table.
#?  @param file_path: The path to the file containing links.
#?  @param postgres_conn_id: The ID of the PostgreSQL connection.
#?  @param site_name: The name of the website.
#?  @param site_id: The ID of the website.
#?  @param site_url: The URL of the website.
#?  @param kwargs: Additional keyword arguments.
#// @return: None

def __insert_all_links__(file_path: str, postgres_conn_id: str, site_name: str, site_id: str, site_url: str, **kwargs):
    conn_hook = PostgresHook(postgres_conn_id=postgres_conn_id)

    with open(file_path, "r") as file:
        links = [line.strip() for line in file.readlines()]

    base_sql = sql.SQL("BEGIN; INSERT INTO website_links (site_name, site_id, site_url, link_url, timestamp) VALUES (%s, %s, %s, %s, CURRENT_TIMESTAMP); END;")

    for link in links:
        parameters = [site_name, site_id, site_url, link]
        insert_command = base_sql.format(sql.Identifier(postgres_conn_id), sql.Literal(site_url))
        conn_hook.run(insert_command, parameters=parameters)


#   Process links and insert headlines into the 'website_headlines' table.
#?  @param option: The specific option for which links are processed.
#?  @param indexes: Explicit list of indexes to process.
#?  @param lower_bound: The lower bound index for processing a subset of links.
#?  @param upper_bound: The upper bound index for processing a subset of links.
#?  @param kwargs: Additional keyword arguments from the Airflow context.
#// @return: None

def __insert_fetched_headlines__(option: str, indexes: Union[List[int], tuple] = None, lower_bound: int = None, upper_bound: int = None):
    with open(f"{option}.txt", "r") as file:
        links = [line.strip() for line in file.readlines()]

    if indexes is not None:
        selected_links = [links[i] for i in indexes if 0 <= i < len(links)]
        start_index = min(indexes)
    elif lower_bound is not None and upper_bound is not None:
        selected_links = links[lower_bound:upper_bound]
        start_index = lower_bound
    else:
        print("Invalid parameters. Please provide either 'indexes' or 'lower_bound' and 'upper_bound'.")
        return

    postgres_hook = PostgresHook(postgres_conn_id='2435676')

    for index, link in enumerate(selected_links, start=start_index):
        print(f"Fetching headline {index}: {link}")

        select_link_id_query = f"SELECT link_id, site_name, timestamp FROM website_links WHERE link_url = %s"
        link_id = postgres_hook.get_first(select_link_id_query, parameters=(link,))[0]
        site_name = postgres_hook.get_first(select_link_id_query, parameters=(link,))[1]
        timestamp = postgres_hook.get_first(select_link_id_query, parameters=(link,))[2]

        insert_headline_query = f"BEGIN; " \
                                f"INSERT INTO website_headlines (headline_url, site_name, timestamp, link_id) " \
                                f"VALUES (%s, %s, %s, %s) " \
                                f"ON CONFLICT (headline_url) DO NOTHING; " \
                                f"END; "
        postgres_hook.run(insert_headline_query, parameters=(link, site_name, timestamp, link_id))

    print("Processing complete.")