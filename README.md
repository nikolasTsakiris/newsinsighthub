# NewsInsightHub

## Project Overview
NewsInsightHub is a dynamic platform that harnesses news data from 24 prominent Greek news sites. This project goes beyond conventional news aggregation by employing data engineering practices to not only aggregate, process, and analyze headlines but also to uncover the diverse perspectives presented by different news sources. Through a comprehensive approach, NewsInsightHub explores interconnections between news articles, shedding light on how the same headline can be portrayed differently across various platforms.

## Features
- Aggregation of headlines from 24 major Greek news sites.
- Robust data engineering methods for efficient storage, processing, and analysis.
- Natural Language Processing (NLP) for extracting insights and sentiment analysis.
- Exploration of personalized news feeds and sentiment trends.
- Highlighting interconnections and disparities between news articles from different sources.

## License
This project is licensed under the [MIT License](LICENSE).

## Acknowledgments
- Special thanks to the Greek news sites for providing valuable data.
