from datetime import datetime
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
import os
import news_utils

path = os.environ.get('NEWS_PATH')
website = "newsbreak"
connection = os.environ.get('DB_CONNECTION')


#   Extract and save NEWSBREAK links to a text file.
#// @param 
#// @return: None

def extract_articles():
    news_utils.__save_links__(website)


#   Fetch a list of links from the NEWSBREAK text file, inserting a subset of them (HEADLINES) into DB.
#// @param 
#// @return: None
    
def insert_headlines():
    db_connection = os.environ.get('DB_CONNECTION')
    news_utils.__insert_fetched_headlines__(website, db_connection, indexes = [218,219,222,226,229])

default_args = {
    'owner': 'nikolasTsakiris',
    'start_date': datetime(2024, 1, 1),
}

dag = DAG(
    '04newsbreak_dag',
    default_args=default_args,
    description='DAG for extracting NEWSBREAK links and fetching the headlines',
    schedule_interval='*/2 * * * *', 
    catchup=False
)

extract_articles_task = PythonOperator(
    task_id='extract_articles_task',
    python_callable=extract_articles,
    provide_context=True,
    dag=dag
)

insert_all_links_task = PythonOperator(
    task_id='insert_all_links_task',
    python_callable=news_utils.__insert_all_links__,
    provide_context=True,
    op_args=[f'{path}/{website}.txt', connection, website, "4", f"https://www.{website}.gr"],
    dag=dag
)

insert_fetched_headlines_task = PythonOperator(
    task_id='insert_fetched_headlines_task',
    python_callable=insert_headlines,
    provide_context=True,
    dag=dag
)

extract_articles_task >> insert_all_links_task >> insert_fetched_headlines_task